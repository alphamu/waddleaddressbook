package com.waddleworks.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * User: Ali
 * Date: 10/01/11
 * Time: 16:51
 */
public class ContactDataHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "wwaddressbook.db";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_DATA_ID = "data_id";
    public static final String COLUMN_DATA_NUMBER = "data_number";
    public static final String COLUMN_CONTACT_DATA = "contact_data";
    public static final String COLUMN_CONTACT_TYPE = "contact_type";
    public static final String COLUMN_CONTACT_DAY = "contact_day";
    public static final String COLUMN_CONTACT_MONTH = "contact_month";
    public static final String COLUMN_CONTACT_YEAR = "contact_year";
    public static final String COLUMN_CONTACT_TIME = "contact_time";
    public static final String COLUMN_CONTACT_TIMESTAMP = "tstamp";

    public static final String COLUMN_CALL_COUNT = "call_count";
    public static final String COLUMN_SMS_COUNT = "sms_count";
    public static final String COLUMN_MMS_COUNT = "mms_count";
    public static final String COLUMN_OTHER_COUNT = "other_count";
    public static final String COLUMN_LAST_CONTACTED = "last_contacted";

    public static final String TABLE_CONTACT_DETAILS = "contact_details";
    public static final String TABLE_CONTACT_SUMMARY = "contact_summary";


    private static final String CONTACT_DETAIL_TABLE =
            "CREATE TABLE "+TABLE_CONTACT_DETAILS+" (" +
                    COLUMN_ID+"                 INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_DATA_ID+"            TEXT, " + //holds the Contact Data Id
                    COLUMN_DATA_NUMBER+"        TEXT NOT NULL, " + //holds the Contact Data Id
                    COLUMN_CONTACT_DATA+"       TEXT, " +  //geo location for example
                    COLUMN_CONTACT_TYPE+"       INTEGER NOT NULL, " +  //call, sms, mms
                    COLUMN_CONTACT_DAY+"        INTEGER DEFAULT (STRFTIME('%d','now','localtime')), " +
                    COLUMN_CONTACT_MONTH+"      INTEGER DEFAULT (STRFTIME('%m','now','localtime')), " +
                    COLUMN_CONTACT_YEAR+"       INTEGER DEFAULT (STRFTIME('%Y','now','localtime')), " +
                    COLUMN_CONTACT_TIME+"       INTEGER DEFAULT ((STRFTIME('%H','now','localtime')*60)+STRFTIME('%M','now','localtime')), " +
                    COLUMN_CONTACT_TIMESTAMP+"  DATETIME DEFAULT (DATETIME('now','localtime'))" +
                    ");";

    private static final String CONTACT_SUMMARY_TABLE =
            "CREATE TABLE "+TABLE_CONTACT_SUMMARY+" (" +
                    COLUMN_ID+"             INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_DATA_ID+"        TEXT, " +
                    COLUMN_DATA_NUMBER+"    TEXT NOT NULL, " +
                    COLUMN_CALL_COUNT+"     INTEGER DEFAULT 0, " +
                    COLUMN_SMS_COUNT+"      INTEGER DEFAULT 0, " +
                    COLUMN_MMS_COUNT+"      INTEGER DEFAULT 0, " +
                    COLUMN_OTHER_COUNT+"    INTEGER DEFAULT 0, " +
                    COLUMN_LAST_CONTACTED+" DATETIME DEFAULT (DATETIME('now','localtime'))" +
                    ");";

    public ContactDataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CONTACT_DETAIL_TABLE);
        db.execSQL(CONTACT_SUMMARY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CONTACT_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CONTACT_SUMMARY);
        onCreate(db);
    }
}
