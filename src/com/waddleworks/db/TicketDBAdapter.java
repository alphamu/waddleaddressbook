package com.waddleworks.db;

/**
 * Created by IntelliJ IDEA.
 * User: Ali
 * Date: 10/01/11
 * Time: 23:19
 * To change this template use File | Settings | File Templates.
 *//*
public class TicketDBAdapter {
    private static final String PARKEDCARS_TABLE = "ParkedCars";
    private static final String PARKINGMETERS_TABLE = "ParkingMeters";
    private static final String PARKINGTICKETS_TABLE = "ParkingTickets";
    private static final String POLICEOFFICERS_TABLE = "PoliceOfficers";

    // The name and column index for each column in PARKEDCARS
    public static final String KEY_CARID = "carID";
    public static final int CARID_COLUMN = 0;
    public static final String KEY_CARMAKE = "Make";
    public static final int CARMAKE_COLUMN = 1;
    public static final String KEY_CARMODEL = "Model";
    public static final int CARMODEL_COLUMN = 2;
    public static final String KEY_CARCOLOR = "Color";
    public static final int CARCOLOR_COLUMN = 3;
    public static final String KEY_CARLICENSENUMBER = "LicenseNumber";
    public static final int CARLICENSENUMBER_COLUMN = 4;
    public static final String KEY_CARMINUTESPARKED = "MinutesParked";
    public static final int CARMINUTESPARKED_COLUMN = 5;

    // The name and column index for each column in PARKINGMETERS
    public static final String KEY_METERID = "meterID";
    public static final int METERID_COLUMN = 0;
    public static final String KEY_MINUTESPURCHASED = "MinutesPurchased";
    public static final int MINUTESPURCHASED_COLUMN = 1;

// The name and column index for each column in PARKINGTICKETS
    //TODO create the columns and indexs for parking tickets

    // The name and column index for each column in POLICEOFFICERS
    public static final String KEY_OFFICERID = "officerID";
    public static final int OFFICERID_COLUMN = 0;
    public static final String KEY_OFFICERNAME = "Name";
    public static final int OFFICERNAME_COLUMN = 1;
    public static final String KEY_OFFICERBADGE = "BadgeNumber";
    public static final int OFFICERBADE_COLUMN = 2;


    //Variable to hold the database instance
    private SQLiteDatabase ticketDB;

    //Context of the application using the database.
    private final Context context;

    //Database open/upgrade helper
    private TicketDBHelper ticketDBHelper;

    public TicketDBAdapter(Context _context) {
        context = _context;
        ticketDBHelper = new TicketDBHelper(context, "DB NAME", null, 1);
    }

    public void open() throws SQLiteException {
        try {
            ticketDB = ticketDBHelper.getWritableDatabase();
        } catch (SQLiteException ex) {
            ticketDB = ticketDBHelper.getReadableDatabase();
        }
    }

    public void close() {
        ticketDB.close();
    }

    //Insert a new ParkedCar
    public long insertParkedCar(ParkedCar _car) {
        //Create a new row of values to insert
        ContentValues newParkedCarValues = new ContentValues();

        //Assign values for each row
        newParkedCarValues.put(KEY_CARMAKE, _car.getMake());
        newParkedCarValues.put(KEY_CARMODEL, _car.getModel());
        newParkedCarValues.put(KEY_CARCOLOR, _car.getColor());
        newParkedCarValues.put(KEY_CARLICENSENUMBER, _car.getLicenseNumber());
        newParkedCarValues.put(KEY_CARMINUTESPARKED, _car.getMinutesParked());

        //Insert the row
        return ticketDB.insert(PARKEDCARS_TABLE, null, newParkedCarValues);
    }

    //Remove a ParkedCar based on its index
    public boolean removeParkedCar(long _rowIndex) {
        return ticketDB.delete(PARKEDCARS_TABLE, KEY_CARID + "=" + _rowIndex, null) > 0;
    }

//Update a ParkedCar's MinutesParked
//TODO Create an update for ParkedCar's minutesParked.

    public Cursor getAllParkedCarsCursor() {
        return ticketDB.query(PARKEDCARS_TABLE, new String[]{KEY_CARID, KEY_CARMAKE, KEY_CARMODEL, KEY_CARCOLOR, KEY_CARLICENSENUMBER, KEY_CARMINUTESPARKED}, null, null, null, null, null);
    }

    public Cursor setCursorParkedCar(long _rowIndex) throws SQLException {
        Cursor result = ticketDB.query(true, PARKEDCARS_TABLE, new String[]{KEY_CARID}, KEY_CARID + "=" + _rowIndex, null, null, null, null, null);

        if ((result.getCount() == 0) || !result.moveToFirst()) {
            throw new SQLException("No ParkedCar found for row: " + _rowIndex);
        }

        return result;
    }

    public static class TicketDBHelper extends SQLiteOpenHelper {
        public TicketDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        //SQL Statement to create PARKEDCARS table
        private static final String PARKEDCARS_CREATE = "create table " + PARKEDCARS_TABLE + " (" + KEY_CARID + " integer primary key autoincrement, " + KEY_CARMAKE + " text not null," + KEY_CARMODEL + " text not null," + KEY_CARCOLOR + " text not null," + KEY_CARLICENSENUMBER + " text not null," + KEY_CARMINUTESPARKED + "int not null);";

        //SQL Statement to create ParkingMeters table
        private static final String PARKINGMETERS_CREATE = "create table" + PARKINGMETERS_TABLE + " (" + KEY_METERID + " integer primary key autoincrement, " + KEY_MINUTESPURCHASED + " int not null);";

        //SQL Statement to create ParkingTickets table
        //TODO create the statement for parkingTickets

        //SQL Statement to create PoliceOfficers table
        private static final String POLICEOFFICERS_CREATE = "create table" + POLICEOFFICERS_TABLE + " (" + KEY_OFFICERID + " integer primary key autoincrement, " + KEY_OFFICERNAME + " text not null," + KEY_OFFICERBADGE + "text not null);";

        //Called when no database exists in disk and the helper class needs to create a new one.
        @Override
        public void onCreate(SQLiteDatabase _db) {
            // _db.execSQL(PARKEDCARS_CREATE); _db.execSQL(PARKINGMETERS_CREATE); // _db.execSQL(POLICEOFFICERS_CREATE);
        }

        //Called when there is a database verion mismatch meaning that the version of the database on disk needs to be upgraded to the current version
        @Override
        public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion) {
            //Log the version upgrade.
            Log.w("TaskDBAdapter", "Upgrading from version " + _oldVersion + " to " + _newVersion + ", which will destroy all old db");

            //Upgrade the existing database to conform to the new version
            //Multiple previous versions can be handled by comparing _oldVersoin and _newVersion values

            //The simplest case is to drop teh old table and create a new one.
            _db.execSQL("DROP TABLE IF EXISTS " + PARKEDCARS_TABLE);
            _db.execSQL("DROP TABLE IF EXISTS " + PARKINGMETERS_TABLE);
            _db.execSQL("DROP TABLE IF EXISTS " + POLICEOFFICERS_TABLE);

            onCreate(_db);
        }
    }
}*/

