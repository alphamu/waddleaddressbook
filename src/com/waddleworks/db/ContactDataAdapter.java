package com.waddleworks.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import com.waddleworks.model.ContactData;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.CommonDataKinds.Phone;

/**
 * User: Ali
 * Date: 11/01/11
 * Time: 09:57
 */
public class ContactDataAdapter {
    //Variable to hold the database instance
    private SQLiteDatabase contactDB;

    //Context of the application using the database.
    private final Context context;

    //Database open/upgrade helper
    private ContactDataHelper contactDataHelper;

    public ContactDataAdapter(Context context) {
        this.context = context;
        contactDataHelper = new ContactDataHelper(context);
    }

    public void open() throws SQLiteException {
        try {
            contactDB = contactDataHelper.getWritableDatabase();
            Log.d("ContactDataHelper", "Opening Writable Database");
        } catch (SQLiteException ex) {
            contactDB = contactDataHelper.getReadableDatabase();
            Log.d("ContactDataHelper", "Opening Readable Database");
        }
    }

    public void close() {
        if (contactDB != null && contactDB.isOpen())
            contactDB.close();
    }

    public long insert(ContactData contactData) {
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(contactData.getDataNumber()));
        Cursor cur = context.getContentResolver().query(contactUri, new String[]{ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER}, null, null, null);
        try {
            if (cur.moveToFirst()) {
                String id = cur.getString(0);
                Log.i("WW", "ID = " + id);
                String number = cur.getString(1);
                Log.i("WW", "Number = " + number);

                Cursor c = context.getContentResolver().query(Phone.CONTENT_URI, new String[]{Data._ID}, Data.DATA1 + " LIKE '" + number + "'", null, null);
                try {
                    if (c.moveToFirst()) {
                        String dataId = c.getString(0);
                        Log.i("WW", "Data ID = " + dataId);
                        contactData.setDataId(dataId);
                        contactData.setDataNumber(number);
                    }
                } finally {
                    if (c != null)
                        c.close();
                }

            }
        } finally {
            if (cur != null)
                cur.close();
        }
        long insert = insertContactData(contactData);
        int update = updateContactSummary(contactData);
        return insert + update;
    }

    public long insertContactData(ContactData contactData) {
        ContentValues contacted = new ContentValues();

        contacted.put(ContactDataHelper.COLUMN_DATA_ID, contactData.getDataId()); //contains the number
        contacted.put(ContactDataHelper.COLUMN_DATA_NUMBER, contactData.getDataNumber()); //contains the number
        contacted.put(ContactDataHelper.COLUMN_CONTACT_DATA, contactData.getContactData());
        contacted.put(ContactDataHelper.COLUMN_CONTACT_TYPE, contactData.getContactType());
        //contacted.put(ContactDataHelper.COLUMN_CONTACT_DAY, contactData.getContactDay());
        //contacted.put(ContactDataHelper.COLUMN_CONTACT_MONTH, contactData.getContactMonth());
        //contacted.put(ContactDataHelper.COLUMN_CONTACT_YEAR, contactData.getContactYear());
        //contacted.put(ContactDataHelper.COLUMN_CONTACT_TIME, contactData.getContactTime());
        //contacted.put(ContactDataHelper.COLUMN_CONTACT_TIMESTAMP, contactData.getTimestamp());

        long insert = contactDB.insert(ContactDataHelper.TABLE_CONTACT_DETAILS, null, contacted);
        return insert;
    }

    public int updateContactSummary(ContactData contactData) {
        ContentValues contacted = new ContentValues();
        contacted.put(ContactDataHelper.COLUMN_DATA_ID, contactData.getDataId());
        contacted.put(ContactDataHelper.COLUMN_DATA_NUMBER, contactData.getDataNumber());
        boolean useNumber = (contactData.getDataId() == null);
        String paramVal = (useNumber)?contactData.getDataNumber():contactData.getDataId();
        String param = (useNumber)? "DATA_NUMBER" : "DATA_ID";

        Cursor c = contactDB.query(
                ContactDataHelper.TABLE_CONTACT_SUMMARY,
                new String[]{ContactDataHelper.COLUMN_CALL_COUNT, ContactDataHelper.COLUMN_SMS_COUNT, ContactDataHelper.COLUMN_MMS_COUNT, ContactDataHelper.COLUMN_OTHER_COUNT, ContactDataHelper.COLUMN_LAST_CONTACTED},
                param + "=?" ,
                new String[]{paramVal},
                null, null, null);

        boolean update = false;

        if (c != null && c.getCount() > 0 && c.moveToFirst()) {
            contacted.put(ContactDataHelper.COLUMN_CALL_COUNT, c.getInt(0));
            contacted.put(ContactDataHelper.COLUMN_SMS_COUNT, c.getInt(1));
            contacted.put(ContactDataHelper.COLUMN_MMS_COUNT, c.getInt(2));
            contacted.put(ContactDataHelper.COLUMN_OTHER_COUNT, c.getInt(3));
            update = true;
        } else {
            contacted.put(ContactDataHelper.COLUMN_DATA_ID, contactData.getDataId());
            contacted.put(ContactDataHelper.COLUMN_DATA_NUMBER, contactData.getDataNumber());
            contacted.put(ContactDataHelper.COLUMN_CALL_COUNT, 0);
            contacted.put(ContactDataHelper.COLUMN_SMS_COUNT, 0);
            contacted.put(ContactDataHelper.COLUMN_MMS_COUNT, 0);
            contacted.put(ContactDataHelper.COLUMN_OTHER_COUNT, 0);
            update = false;
        }
        switch (contactData.getContactType()) {
            case ContactData.TYPE_CALL:
                contacted.put(ContactDataHelper.COLUMN_CALL_COUNT, ((Integer) contacted.get(ContactDataHelper.COLUMN_CALL_COUNT)) + 1);
                break;
            case ContactData.TYPE_SMS:
                contacted.put(ContactDataHelper.COLUMN_SMS_COUNT, ((Integer) contacted.get(ContactDataHelper.COLUMN_SMS_COUNT)) + 1);
                break;
            case ContactData.TYPE_MMS:
                contacted.put(ContactDataHelper.COLUMN_MMS_COUNT, ((Integer) contacted.get(ContactDataHelper.COLUMN_MMS_COUNT)) + 1);
                break;
            case ContactData.TYPE_OTHER:
                contacted.put(ContactDataHelper.COLUMN_OTHER_COUNT, ((Integer) contacted.get(ContactDataHelper.COLUMN_OTHER_COUNT)) + 1);
                break;
            default:
                break;
        }
        contacted.put(ContactDataHelper.COLUMN_LAST_CONTACTED, contactData.getTimestamp().toString());
        int result = 0;
        if (update)
            result = contactDB.update(ContactDataHelper.TABLE_CONTACT_SUMMARY, contacted, ContactDataHelper.COLUMN_DATA_ID + "=?", new String[]{contactData.getDataId()});
        else
            result = ((Long) contactDB.insert(ContactDataHelper.TABLE_CONTACT_SUMMARY, null, contacted)).intValue();
        return result;
    }

    public Cursor getCursorForContactDetails(String dataId) {
        Cursor result = contactDB.query(ContactDataHelper.TABLE_CONTACT_DETAILS,
                null,
                ContactDataHelper.COLUMN_DATA_ID + "=?",
                new String[]{dataId},
                null,
                null,
                null);

        if ((result.getCount() == 0) || !result.moveToFirst()) {
            Log.e("ContactDataAdapter", "No Detail found for: " + dataId);
            throw new SQLException("No Details found for: " + dataId);
        }
        return result;

    }

    public Cursor getCursorForAllContactDetails() {
        return contactDB.query(ContactDataHelper.TABLE_CONTACT_DETAILS,
                null,
                null,
                null,
                null,
                null,
                null);
    }

    public Cursor getCursorForContactSummary(String dataId) {
        Cursor result = contactDB.query(ContactDataHelper.TABLE_CONTACT_SUMMARY,
                null,
                ContactDataHelper.COLUMN_DATA_ID + "=?",
                new String[]{dataId},
                null,
                null,
                null);
        if ((result.getCount() == 0) || !result.moveToFirst()) {
            Log.e("ContactDataAdapter", "No Summary found for: " + dataId);
            throw new SQLException("No Summary found for: " + dataId);
        }
        return result;
    }

    public Cursor getCursorForAllContactSummary() {
        return contactDB.query(ContactDataHelper.TABLE_CONTACT_SUMMARY,
                null,
                null,
                null,
                null,
                null,
                null);
    }

    public int removeContactDetails(String dataId) {
        return contactDB.delete(ContactDataHelper.TABLE_CONTACT_DETAILS, ContactDataHelper.COLUMN_DATA_ID + "=?", new String[]{dataId});
    }

    public int removeContactSummary(String dataId) {
        return contactDB.delete(ContactDataHelper.TABLE_CONTACT_SUMMARY, ContactDataHelper.COLUMN_DATA_ID + "=?", new String[]{dataId});
    }

    public int removeAllContactData(String dataId) {
        int rows = 0;
        rows += removeContactDetails(dataId);
        rows += removeContactSummary(dataId);
        return rows;
    }

}
