package com.waddleworks.model;

import java.sql.Timestamp;
import java.util.concurrent.Callable;

/**
 * User: Ali
 * Date: 11/01/11
 * Time: 10:07
 */
public class ContactData {

    public static final int TYPE_CALL = 0;
    public static final int TYPE_SMS = 1;
    public static final int TYPE_MMS = 2;
    public static final int TYPE_OTHER = 3;

    private String dataId = null;
    private String dataNumber;
    private String contactData;
    private Integer contactType;
    private Integer contactDay;
    private Integer contactMonth;
    private Integer contactYear;
    private Integer contactTime;
    private Timestamp timestamp;

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getContactData() {
        return contactData;
    }

    public void setContactData(String contactData) {
        this.contactData = contactData;
    }

    public Integer getContactType() {
        return contactType;
    }

    public void setContactType(Integer contactType) {
        this.contactType = contactType;
    }

    public Integer getContactDay() {
        return contactDay;
    }

    public void setContactDay(Integer contactDay) {
        this.contactDay = contactDay;
    }

    public Integer getContactMonth() {
        return contactMonth;
    }

    public void setContactMonth(Integer contactMonth) {
        this.contactMonth = contactMonth;
    }

    public Integer getContactYear() {
        return contactYear;
    }

    public void setContactYear(Integer contactYear) {
        this.contactYear = contactYear;
    }

    public Integer getContactTime() {
        return contactTime;
    }

    public void setContactTime(Integer contactTime) {
        this.contactTime = contactTime;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getDataNumber() {
        return dataNumber;
    }

    public void setDataNumber(String dataNumber) {
        this.dataNumber = dataNumber;
    }
}
