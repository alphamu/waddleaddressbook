package com.waddleworks.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * User: Ali
 * Date: 09/01/11
 * Time: 14:17
 */
public class SMSReceivedBroadcastReceiver extends BroadcastReceiver{
    /* package */ static final String SMS_RECEIVED =  "android.provider.Telephony.SMS_RECEIVED";
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(action.equals(SMS_RECEIVED)){
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs = null;
            String originateAddress = "";
            String messagebody = "";

            if (bundle != null)
            {
                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];
                for (int i=0; i<msgs.length; i++)
                {
                    msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                    originateAddress=msgs[i].getOriginatingAddress();
                    messagebody=msgs[i].getMessageBody().toString();
                    Log.i("WW", "From :" + originateAddress);
                    Log.i("WW", "Body ::"+messagebody);
                }

            }

        }
    }
}
