package com.waddleworks.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import com.waddleworks.db.ContactDataAdapter;
import com.waddleworks.model.ContactData;

import java.sql.Timestamp;

/**
 * User: Ali
 * Date: 09/01/11
 * Time: 11:02
 */
public class OutgoingCallBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Intent.ACTION_NEW_OUTGOING_CALL.equals(action)) {
            String number = getResultData();
            Log.i("WW","Number is "+number);
            ContactData cd=new ContactData();
            cd.setDataNumber(number) ;
            cd.setContactType(ContactData.TYPE_CALL);
            cd.setTimestamp(new Timestamp(System.currentTimeMillis()));
            ContactDataAdapter data = new ContactDataAdapter(context);
            try{
                data.open();
                data.insert(cd);
            }catch(SQLiteException e){
                Log.e("OutgoingCallBroadcastReceiver",e.getMessage(),e);
            } finally{
                data.close();
            }

        } else if(Intent.ACTION_ANSWER.equals(action)){
            String number = getResultData();
            Log.i("WW","Number is "+number);
        }
    }
}
