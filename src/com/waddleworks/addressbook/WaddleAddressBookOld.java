package com.waddleworks.addressbook;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

/**
 * Created by IntelliJ IDEA.
 * User: Ali
 * Date: 14-May-2010
 * Time: 16:08:53
 * To change this template use File | Settings | File Templates.
 */
public class WaddleAddressBookOld extends ListActivity {

    /**
     * Called when the activity is first created.
     */
    private ListAdapter mAdapter;
    private static final int PICK_CONTACT = 3;

    /**
     * Called when the activity is first created.
     *//*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Cursor c = getContentResolver().query(Contacts.People.CONTENT_URI, null, null, null, null);
        startManagingCursor(c);

        String[] columns = new String[]{Contacts.Phones.NAME};
        int[] names = new int[]{R.id.row_entry};

        mAdapter = new SimpleCursorAdapter(this,
                R.layout.main, c, columns, names);
        setListAdapter(mAdapter);

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent i = new Intent(Intent.ACTION_CALL);

        Cursor c = (Cursor) mAdapter.getItem(position);
        int columnIndex = c.getColumnIndex(Contacts.People.PRIMARY_PHONE_ID);
        String phoneId = c.getString(columnIndex);
        if (phoneId == null) {
            columnIndex = c.getColumnIndex(Contacts.People._ID);
            String personId = c.getString(columnIndex);
            Intent i2 = new Intent(Intent.ACTION_VIEW);
            i2.setData(Uri.withAppendedPath(Contacts.People.CONTENT_URI,personId));
            startActivityForResult(i2, PICK_CONTACT);
            return;
        }

        i.setData(Uri.withAppendedPath(Contacts.Phones.CONTENT_URI, phoneId));
        startActivity(i);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);

                    if (c.moveToFirst()) {
                        // other data is available for the Contact.  I have decided
                        //    to only get the name of the Contact.
                        String name = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
                    }
                }
        }
    }*/
}

