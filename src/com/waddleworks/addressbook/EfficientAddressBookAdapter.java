package com.waddleworks.addressbook;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.*;
import android.widget.*;
import com.waddleworks.R;

import java.io.InputStream;

/**
 * User: Ali
 * Date: 05/01/11
 * Time: 04:40
 */
class EfficientAddressBookAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private Bitmap mIcon1;
    private Cursor c;
    private Context context;

    public EfficientAddressBookAdapter(Context context, Cursor c) {
        // Cache the LayoutInflate to avoid asking for a new one each time.
        mInflater = LayoutInflater.from(context);
        this.context = context;

        // Icons bound to the rows.
        mIcon1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon);
        this.c = c;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        return c.getCount();
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficent to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {

        if (c.moveToPosition(position)) {
            return c;
        }
        return null;
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        Cursor c = (Cursor) getItem(position);
        return c.getLong(1);
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    public void requery() {
        c.requery();
    }

    public void close(){
        notifyDataSetInvalidated();
        notifyDataSetChanged();
        c.close();
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, android.view.View,
     *      android.view.ViewGroup)
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unneccessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.search, null);

            // Creates a ViewHolder and store references to the two children views
            // we want to bind data to.
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.firstLine);
            holder.phone = (TextView) convertView.findViewById(R.id.secondLine);
            holder.call = (ImageButton) convertView.findViewById(R.id.btnCall);
            holder.sms = (ImageButton) convertView.findViewById(R.id.btnSms);

            convertView.setTag(holder);
        } else {
            // Get the ViewHolder back to get fast access to the TextView
            // and the ImageView.
            holder = (ViewHolder) convertView.getTag();
        }

        // Bind the data efficiently with the holder.
        final Cursor temp = (Cursor) getItem(position);

        //int index = temp.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int ct = temp.getInt(WaddleAddressBook.CONTACT_TYPE);
        String numberType = "Other";
        switch (ct) {
            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                numberType = "Home";
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                numberType = "Mobile";
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                numberType = "Work";
                break;
        }
        holder.name.setText(temp.getString(WaddleAddressBook.DISPLAY_NAME));
        holder.phone.setText(numberType + ": " + temp.getString(WaddleAddressBook.PHONE_NO) + " (" +
                temp.getString(WaddleAddressBook.TIMES_CONTACTED) + ")");
        //holder.icon.setImageBitmap(mIcon1);

        holder.call.setClickable(true);
        holder.call.setFocusable(true);
        holder.call.setTag("call");
        final int pos = position;

        holder.call.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Toast.makeText(context, "CALL", Toast.LENGTH_SHORT).show();
                Cursor c = (Cursor) getItem(pos);
                Intent i = new Intent(Intent.ACTION_CALL);
                String phoneNumber = c.getString(WaddleAddressBook.DATA_ID);
                i.setData(Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, phoneNumber));
                context.startActivity(i);
            }
        });

        holder.sms.setClickable(true);
        holder.sms.setFocusable(true);
        holder.sms.setTag("sms");
        holder.sms.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Toast.makeText(context, "SMS", Toast.LENGTH_SHORT).show();
                Cursor c = (Cursor) getItem(pos);
                String phoneNumber = c.getString(WaddleAddressBook.PHONE_NO);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.putExtra("address", phoneNumber);
                intent.setType("vnd.android-dir/mms-sms");
                context.startActivity(intent);
            }
        });


        return convertView;
    }

    public static Bitmap loadContactPhoto(ContentResolver cr, long id) {
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, uri);
        if (input == null) {
            return null;
        }
        return BitmapFactory.decodeStream(input);
    }

    static class ViewHolder {
        TextView name, phone;
        ImageButton call, sms, other;
        //ImageView icon;
    }
}
