package com.waddleworks.addressbook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.*;
import android.widget.*;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.widget.AdapterView;
import com.waddleworks.R;


/**
 * User: Ali
 * Date: 14-May-2010
 * Time: 16:08:53
 */
public class WaddleAddressBook extends Activity {

    /**
     * Called when the activity is first created.
     */
    protected static final int CONTEXT_ADD = 0;
    protected static final int CONTEXT_EDIT = 1;
    protected static final int CONTEXT_DELETE = 2;
    private ListAdapter mAdapter;

    private static final int PICK_CONTACT = 3;
    private static final int DELETE_CONTACT = 4;
    private static final int ADD_CONTACT = 5;
    private static final int EDIT_CONTACT = 6;
    private static final int DIAL_CONTACT = 7;
    private static final int SMS_CONTACT = 8;

    private GestureDetector gestureDetector;
    private View.OnTouchListener gestureListener;
    ListView lv;
    LinearLayout ll;
    EditText search;
    int textlength = 0;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        lv = (ListView) findViewById(R.id.contacts);
        ll = (LinearLayout) findViewById(R.layout.main);
        Cursor c = query(Data.DATA1 + " IS NOT NULL AND " + Data.MIMETYPE + "=?",
                new String[]{String.valueOf(Phone.CONTENT_ITEM_TYPE)},
                Data.TIMES_CONTACTED + " DESC"
        );

        startManagingCursor(c);
        mAdapter = new EfficientAddressBookAdapter(this, c);
        lv.setAdapter(mAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(WaddleAddressBook.this, "This should select item", Toast.LENGTH_SHORT).show();
                onListItemClick(adapterView, view, i, l);
            }
        });

        gestureDetector = new GestureDetector(new ListItemGestureDetector(this));
        gestureListener = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (gestureDetector.onTouchEvent(event)) {
                    return true;
                }
                return false;
            }
        };
        lv.setOnTouchListener(gestureListener);

        lv.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
                contextMenu.setHeaderTitle("Actions");
                contextMenu.add(0, CONTEXT_ADD, 0, "Add New Contact");
                contextMenu.add(0, CONTEXT_EDIT, 0, "Edit Selected Contact");
                contextMenu.add(0, CONTEXT_DELETE, 0, "Delete Selected Contact");
                /* Add as many context-menu-options as you want to. */
            }
        });


        search = (EditText) findViewById(R.id.search);

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                Cursor c = query(Data.DATA1 + " IS NOT NULL AND " + Data.MIMETYPE + "=? AND " + Phone.DISPLAY_NAME + " LIKE ?",
                        new String[]{String.valueOf(Phone.CONTENT_ITEM_TYPE), "%" + search.getText().toString() + "%"},
                        Data.TIMES_CONTACTED + " DESC"
                );
                startManagingCursor(c);
                mAdapter = new EfficientAddressBookAdapter(WaddleAddressBook.this, c);
                lv.setAdapter(mAdapter);
            }
        });
    }

    // ===========================================================
    // Methods from SuperClass/Interfaces
    // ===========================================================

    @Override
    public boolean onContextItemSelected(MenuItem aItem) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) aItem.getMenuInfo();
        Intent intent = null;
        Cursor c = (Cursor) lv.getAdapter().getItem(menuInfo.position);

        /* Switch on the ID of the item, to get what the user selected. */
        switch (aItem.getItemId()) {
            case CONTEXT_ADD:
                intent = new Intent(Intent.ACTION_INSERT);
                intent.setType(Contacts.CONTENT_TYPE);
                startActivityForResult(intent, ADD_CONTACT);
                break;
            case CONTEXT_EDIT:
                intent = new Intent(Intent.ACTION_EDIT);
                intent.setType(Contacts.CONTENT_TYPE);
                intent.setData(Uri.withAppendedPath(Contacts.CONTENT_URI, c.getString(CONTACT_ID)));
                startActivityForResult(intent, EDIT_CONTACT);
                break;
            case CONTEXT_DELETE:
                getContentResolver().delete(Data.CONTENT_URI, Data._ID + "=?", new String[]{c.getString(DATA_ID)});
                refresh();
                break;
            default:
                break;

        }
        return false;
    }

    //@Override
    protected void onListItemClick(AdapterView l, View v, int position, long id) {
        //    super.onListItemClick(l, v, position, id);

        Intent i = null;

        Cursor c = (Cursor) mAdapter.getItem(position);
        //int columnIndex = c.getColumnIndex(ContactsContract.Contacts._ID);

        if (false) {
            i = new Intent(Intent.ACTION_CALL);
            String phoneNumber = c.getString(DATA_ID);
            i.setData(Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, phoneNumber));
            startActivity(i);
        } else {
            //if no primary number selected then show contact and let them select.
            i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, c.getString(CONTACT_ID)));
            startActivityForResult(i, PICK_CONTACT);
        }

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, new String[]{Contacts.DISPLAY_NAME}, null, null, null);

                    if (c.moveToFirst()) {
                        // other data is available for the Contact.  I have decided
                        //    to only get the name of the Contact.
                        String name = c.getString(0);
                    }
                    refresh();
                }
                break;
            case (DIAL_CONTACT):
            case (SMS_CONTACT):
            case (EDIT_CONTACT):
            case (ADD_CONTACT):
            case (DELETE_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    refresh();
                }
        }
    }

    String lastQuery = null;
    String lastOrder = null;
    String[] lastParams = null;

    private Cursor query(String query, String[] params, String order) {
        lastQuery = query;
        lastOrder = order;
        lastParams = params;
        return getContentResolver().query(Phone.CONTENT_URI,
                new String[]{Data.DISPLAY_NAME, Data._ID, Data.DATA1, Data.TIMES_CONTACTED, Data.LOOKUP_KEY, Data.CONTACT_ID, Phone.TYPE},
                query,
                params,
                order);

    }

    private void refreshQuery() {
        Cursor c = query(lastQuery, lastParams, lastOrder);
        startManagingCursor(c);
        mAdapter = new EfficientAddressBookAdapter(WaddleAddressBook.this, c);
        lv.setAdapter(mAdapter);
    }

    private void refresh() {
        ((EfficientAddressBookAdapter) mAdapter).requery();
        ((EfficientAddressBookAdapter) mAdapter).notifyDataSetChanged();
        lv.refreshDrawableState();
    }

    public static final int DISPLAY_NAME = 0;
    public static final int DATA_ID = 1;
    public static final int PHONE_NO = 2;
    public static final int TIMES_CONTACTED = 3;
    public static final int LOOKUP_KEY = 4;
    public static final int CONTACT_ID = 5;
    public static final int CONTACT_TYPE = 6;


    /////////////////////////////////////////////////////////////
    //InnerClass CLASS GestureDetector LISTENER
    ////////////////////////////////////////////////////////////

    class ListItemGestureDetector extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_MAX_OFF_PATH = 250;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;
        private Context context;

        public ListItemGestureDetector(Context context) {
            this.context = context;
        }


        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    Toast.makeText(context, "Swipe Left Detected: SMSing", Toast.LENGTH_SHORT).show();
                    Cursor c = (Cursor) mAdapter.getItem(lv.pointToPosition((int) e1.getX(), (int) e1.getY()));
                    String phoneNumber = c.getString(PHONE_NO);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.putExtra("address", phoneNumber);
                    intent.setType("vnd.android-dir/mms-sms");
                    startActivityForResult(intent,SMS_CONTACT);

                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    Toast.makeText(context, "Swipe Right Detected: Calling", Toast.LENGTH_SHORT).show();
                    Cursor c = (Cursor) mAdapter.getItem(lv.pointToPosition((int) e1.getX(), (int) e1.getY()));
                    Intent i = new Intent(Intent.ACTION_CALL);
                    String phoneNumber = c.getString(DATA_ID);
                    i.setData(Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, phoneNumber));
                    startActivityForResult(i,DIAL_CONTACT);
                }
            } catch (Exception e) {
                // nothing
            }
            return true;
        }


    }

}

